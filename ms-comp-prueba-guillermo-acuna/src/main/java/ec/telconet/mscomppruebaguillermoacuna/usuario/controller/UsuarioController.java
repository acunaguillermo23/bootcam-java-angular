package ec.telconet.mscomppruebaguillermoacuna.usuario.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ec.telconet.mscomppruebaguillermoacuna.usuario.entity.model.UsuarioEntity;
import ec.telconet.mscomppruebaguillermoacuna.usuario.entity.request.UsuarioRequest;
import ec.telconet.mscomppruebaguillermoacuna.usuario.entity.response.UsuarioResponse;
import ec.telconet.mscomppruebaguillermoacuna.usuario.service.UsuarioService;
import ec.telconet.mscomppruebaguillermoacuna.util.entity.OutputEntity;

@RestController
@RequestMapping("/user")
public class UsuarioController {

	@Autowired
	private UsuarioService usuarioService;

	@GetMapping
	public ResponseEntity<OutputEntity<List<UsuarioResponse>>> getAll() {
		OutputEntity<List<UsuarioResponse>> out = null;
		try {
			out = this.usuarioService.getAll();
			return new ResponseEntity<>(out, out.getCode());
		} catch (Exception e) {
			out = new OutputEntity<List<UsuarioResponse>>().error();
			return new ResponseEntity<>(out, out.getCode());
		}
	}

	@GetMapping("/name/{name}")
	public ResponseEntity<OutputEntity<List<UsuarioResponse>>> findByName(@PathVariable String name) {
		OutputEntity<List<UsuarioResponse>> out = null;
		try {
			out = this.usuarioService.findByName(name);
			return new ResponseEntity<>(out, out.getCode());
		} catch (Exception e) {
			out = new OutputEntity<List<UsuarioResponse>>().error();
			return new ResponseEntity<>(out, out.getCode());
		}
	}

	@GetMapping("/user-name/{user}")
	public ResponseEntity<OutputEntity<List<UsuarioResponse>>> findByUser(@PathVariable String user) {
		OutputEntity<List<UsuarioResponse>> out = null;
		try {
			out = this.usuarioService.findByUser(user);
			return new ResponseEntity<>(out, out.getCode());
		} catch (Exception e) {
			out = new OutputEntity<List<UsuarioResponse>>().error();
			return new ResponseEntity<>(out, out.getCode());
		}
	}

	@GetMapping("/mail/{mail}")
	public ResponseEntity<OutputEntity<List<UsuarioResponse>>> findByMail(@PathVariable String mail) {
		OutputEntity<List<UsuarioResponse>> out = null;
		try {
			out = this.usuarioService.findByMail(mail);
			return new ResponseEntity<>(out, out.getCode());
		} catch (Exception e) {
			out = new OutputEntity<List<UsuarioResponse>>().error();
			return new ResponseEntity<>(out, out.getCode());
		}
	}

	@GetMapping("/page")
	public ResponseEntity<OutputEntity<List<UsuarioResponse>>> getLastUser(@RequestParam Integer paginaInicial,
			@RequestParam Integer paginaFinal) {
		OutputEntity<List<UsuarioResponse>> out = null;
		try {
			out = this.usuarioService.getLastUser(paginaInicial, paginaFinal);
			return new ResponseEntity<>(out, out.getCode());
		} catch (Exception e) {
			out = new OutputEntity<List<UsuarioResponse>>().error();
			return new ResponseEntity<>(out, out.getCode());
		}
	}

	@PostMapping
	public ResponseEntity<OutputEntity<String>> create(@RequestBody UsuarioRequest data) {
		OutputEntity<String> out = null;
		try {
			out = this.usuarioService.create(data);
			return new ResponseEntity<>(out, out.getCode());
		} catch (Exception e) {
			out = new OutputEntity<String>().error();
			return new ResponseEntity<>(out, out.getCode());
		}
	}

}
