package ec.telconet.mscomppruebaguillermoacuna.restTemplate;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import ec.telconet.mscomppruebaguillermoacuna.util.exception.MyException;

@Service
public class ServiceRestTemplate {

    private final RestTemplate restTemplate;
    private final HttpHeaders headers;

    
   
    @Autowired
    public ServiceRestTemplate( RestTemplate restTemplat) {
        this.restTemplate = restTemplat;
        this.headers = new HttpHeaders();
    }

    public void setHeaderRestClient(MultiValueMap<String, String> headerMultiValue) {
        this.headers.addAll(headerMultiValue);
    }

    public void setHeaderContentType(MediaType mediaType) {
        this.headers.setContentType(mediaType);
    }

    public void setHeaderContentTypeDefault() {
        this.headers.setContentType(MediaType.APPLICATION_JSON);
    }

    public void setHeaderContentTypeFormData() {
        this.headers.setContentType(MediaType.MULTIPART_FORM_DATA);
    }

   

    public <T> T getApiRest(Class<T> responseType, @NonNull String url, Map<String, Object> params) throws MyException {
        ResponseEntity<T> responseEntity;
        HttpEntity<T> entity = new HttpEntity<>(this.headers);
        if (params == null) {
            responseEntity = this.restTemplate.exchange(url, HttpMethod.GET, entity, responseType);
        } else {
            responseEntity = this.restTemplate.exchange(url, HttpMethod.GET, entity, responseType, params);
        }

        return responseEntity.getBody();
    }
}
