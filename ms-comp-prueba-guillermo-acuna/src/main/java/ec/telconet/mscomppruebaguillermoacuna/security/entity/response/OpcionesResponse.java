package ec.telconet.mscomppruebaguillermoacuna.security.entity.response;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class OpcionesResponse {

	// TODO LOS CAMPOS, DE LAS OPCIONES DEL SISTEMA
	private String label;
	private String icon;
	private List<ItemsResponse> items = new ArrayList<>();
}
