package ec.telconet.mscomppruebaguillermoacuna.security.entity.response;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class ItemsResponse {
	private String label;
	private String icon;
	private List<String> routerLink = new ArrayList<>();
}
