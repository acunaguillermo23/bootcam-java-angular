package ec.telconet.mscomppruebaguillermoacuna.security.entity.response;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;


@Configuration
@ConfigurationProperties(prefix = "menus")
@Data
public class MenuResponse {

	private List<OpcionesResponse> options = new ArrayList<>();
}
