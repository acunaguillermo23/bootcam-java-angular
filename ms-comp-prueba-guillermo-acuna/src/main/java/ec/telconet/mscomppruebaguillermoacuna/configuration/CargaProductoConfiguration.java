package ec.telconet.mscomppruebaguillermoacuna.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ec.telconet.mscomppruebaguillermoacuna.producto.entity.response.ProductoResponse;
import ec.telconet.mscomppruebaguillermoacuna.restTemplate.ServiceRestTemplate;
import ec.telconet.mscomppruebaguillermoacuna.util.exception.MyException;

@Configuration
public class CargaProductoConfiguration {

	@Value("${ec.url-producto}")
	private String url;

	@Autowired
	private ServiceRestTemplate serviceRest;

	@Bean
	public ProductoResponse insertUserRest() throws MyException {
		ProductoResponse productoResponse = this.serviceRest.getApiRest(ProductoResponse.class, this.url, null);
		System.out.print("respuesta: "+ productoResponse.toString());
		return null;
	}
}
