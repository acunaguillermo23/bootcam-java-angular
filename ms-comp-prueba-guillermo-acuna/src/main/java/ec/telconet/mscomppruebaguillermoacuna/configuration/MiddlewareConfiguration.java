package ec.telconet.mscomppruebaguillermoacuna.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import ec.telconet.mscomppruebaguillermoacuna.middleware.Middleware;

@Component
public class MiddlewareConfiguration implements WebMvcConfigurer{
	
	@Autowired
	private Middleware middleware;
	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(middleware);
	}

}
