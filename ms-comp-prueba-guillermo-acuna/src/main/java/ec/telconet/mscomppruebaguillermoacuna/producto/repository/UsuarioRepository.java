package ec.telconet.mscomppruebaguillermoacuna.producto.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.telconet.mscomppruebaguillermoacuna.usuario.entity.model.UsuarioEntity;

@Repository
public interface UsuarioRepository extends JpaRepository<UsuarioEntity, Integer>{

	@Query(value = "SELECT * FROM BOOTCAM.usuario WHERE name  LIKE %:name%", nativeQuery = true)
	List<UsuarioEntity> findByName(String name);

	
	@Query(value = "SELECT * FROM BOOTCAM.usuario WHERE usarname  LIKE %:user% ", nativeQuery = true)
	List<UsuarioEntity> findByUser(String user);

	@Query(value = "SELECT * FROM BOOTCAM.usuario WHERE usarname  = :user", nativeQuery = true)
	UsuarioEntity findByUserLogin(String user);

	
	@Query(value = "SELECT * FROM BOOTCAM.usuario WHERE mail  = ?1 ", nativeQuery = true)
	List<UsuarioEntity> findByMail(String mail);
	
	
	/*@Query(value = "select * from BOOTCAM.usuario where name LIKE %?1%",nativeQuery = true)
    List<UsuarioEntity> findByName(String name);*/
}
