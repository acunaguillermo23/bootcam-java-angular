package ec.telconet.mscomppruebaguillermoacuna.producto.entity.response;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class ProductoPropiedadesResponse {

	private Integer id;
	private String title;
	private String description;
	private Double price;
	private Double discountPercentage;
	private Double rating;
	private Integer stock;
	private String brand;
	private String category;
	private String thumbnail;
	private List<String> images = new ArrayList<>();
}
