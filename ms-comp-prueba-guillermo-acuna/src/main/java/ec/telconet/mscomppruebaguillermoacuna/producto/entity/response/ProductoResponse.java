package ec.telconet.mscomppruebaguillermoacuna.producto.entity.response;

import java.util.ArrayList;

import lombok.Data;

@Data
public class ProductoResponse {
	 private ArrayList<ProductoPropiedadesResponse> products = new ArrayList<>();
}
