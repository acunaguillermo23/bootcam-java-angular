import { Component, EventEmitter, Input, Output } from '@angular/core';

enum BlockView {
    PREVIEW,
    CODE,
}

@Component({
    // eslint-disable-next-line @angular-eslint/component-selector
    selector: 'block-viewer',
    template: `
        <div class="block-section">
            <div class="block-header">
                <span class="block-title">
                    <span>{{ strHeader }}</span>
                </span>
                <div class="block-actions">
                    <div class="block-actions" *ngIf="flagButton1">
                        <p-button
                            [label]="strLabelButton1"
                            styleClass="p-button-rounded"
                            (click)="onClickButton1()"
                        >

                        </p-button>
                    </div>
                </div>
            </div>
            <div class="block-content">
                <div
                    [class]="containerClass"
                    [ngStyle]="previewStyle"
                    *ngIf="blockView === BlockView.PREVIEW"
                >
                    <ng-content></ng-content>
                </div>               
            </div>
        </div>
    `,
    styleUrls: ['./blockviewer.component.scss'],
})
export class BlockViewerComponent {
    
    @Input() strHeader!: string;

    @Input() strContainerClass!: string;

    @Input() objPreviewStyle!: object;

    @Input() strLabelButton1!: string;

    @Input() flagButton1!: boolean;

    @Output() eventButton1 = new EventEmitter<string>();

    BlockView = BlockView;

    blockView: BlockView = BlockView.PREVIEW;

    onClickButton1(){
        this.eventButton1.emit("ejemplo");
    }
}
