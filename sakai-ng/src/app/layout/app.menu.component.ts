import { OnInit } from '@angular/core';
import { Component } from '@angular/core';
import { LayoutService } from './service/app.layout.service';
import { AuthService } from '../modules/pages/auth/login/services/auth.service';
import { OptionsI } from '../modules/pages/auth/login/interfaces/login.interface';

@Component({
    selector: 'app-menu',
    templateUrl: './app.menu.component.html'
})
export class AppMenuComponent implements OnInit {

    model: OptionsI[] = [];

    constructor(public layoutService: LayoutService, private _authService: AuthService) { }

    ngOnInit() {
        this.model = this._authService.getOpciones();
    }
}
