import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AppLayoutComponent } from './layout/app.layout.component';

@NgModule({
    imports: [
        RouterModule.forRoot(
            [
                {
                    path: '',
                    loadChildren: () =>
                        import('./modules/pages/auth/auth.module').then(
                            (m) => m.AuthModule
                        ),
                },
                {
                    path: 'dashboard',
                    component: AppLayoutComponent,
                    children: [
                        {
                            path: '',
                            loadChildren: () =>
                                import(
                                    './modules/pages/dashboard/dashboard.module'
                                ).then((m) => m.DashboardModule),
                        },
                    ],
                },
                {
                    path: 'manteniminento',
                    component: AppLayoutComponent,
                    children: [
                        {
                            path: '',
                            loadChildren: () =>
                                import(
                                    './modules/pages/maintenance/maintenance.module'
                                ).then((m) => m.MaintenanceModule),
                        },
                    ],
                },
            ],
            {
                scrollPositionRestoration: 'enabled',
                anchorScrolling: 'enabled',
                onSameUrlNavigation: 'reload',
            }
        ),
    ],
    exports: [RouterModule],
})
export class AppRoutingModule {}
