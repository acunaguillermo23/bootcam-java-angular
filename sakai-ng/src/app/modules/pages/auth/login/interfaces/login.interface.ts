export interface LoginRequesI {
    usuario: string;
    clave: string;
}


export interface LoginResponseI {
    mensaje: string[];
    error: boolean;
    data: DataI;
}

export interface DataI {
    usuario: UsuarioI;
    token: string;
    opciones : OptionsI[]
}

export interface UsuarioI {
    identificador: number;
    nombreUsuario: string;
    apellidoUsuario: string;
    usuario: string;
    correo: string;
    estado: string;
    administrador: boolean;
}


export interface OptionsI {
    label: string;
    icon: string;
    items: {
        label: string;
        icon: string;
        routerLink: string[];
    }[];
}