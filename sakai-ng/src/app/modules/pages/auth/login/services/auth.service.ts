import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { LoginRequesI, LoginResponseI, OptionsI, UsuarioI } from '../interfaces/login.interface';
import { Observable } from 'rxjs';
import * as CryptoJS from 'crypto-js';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _isAuthenticated = false;
  private _strUrl: string = environment.api.url;
  private _strKeyToken: string = environment.localStorage.keyToken;
  private _strKeyUsuario: string = environment.localStorage.keyUsuario;
  private _strKeyOpciones: string = environment.localStorage.keyOpciones;
  private _strToken: string = environment.localStorage.token;
  private _strUser: string = environment.localStorage.user;
  private _strOpciones: string = environment.localStorage.opciones;

  constructor(private _http: HttpClient) { }

  isAuthenticated(){
    return this.isAuthenticated;
  }

  authenticated(flag: boolean){
    this._isAuthenticated = flag;
  }

  login(body: LoginRequesI): Observable<LoginResponseI>{
    return  this._http.post<LoginResponseI>(`${this._strUrl}/security`, body);
  }


  saveToken(token: string): void {
    const encryToken = CryptoJS.AES.encrypt(
      token, 
      this._strKeyToken
    ).toString();
    localStorage.setItem(environment.localStorage.token, encryToken);
  }

  getToken(): string{
    var descripToken: string = '';
    const encryToken = localStorage.getItem(environment.localStorage.token);
    if(encryToken){
      descripToken = CryptoJS.AES.decrypt(
        encryToken,
        this._strKeyToken
      ).toString(CryptoJS.enc.Utf8);
    }
    return descripToken;
  }

  saveUser(user: UsuarioI){
    const encryToken = CryptoJS.AES.encrypt(
      JSON.stringify(user), 
      this._strKeyUsuario
    ).toString();
    localStorage.setItem(this._strUser, encryToken);
  }

  getUser(): UsuarioI | null{
    const encryptInfo = localStorage.getItem(this._strUser);
    if(encryptInfo){
      var descripInfo = CryptoJS.AES.decrypt(
        encryptInfo,
        this._strKeyUsuario
      ).toString(CryptoJS.enc.Utf8);
      return JSON.parse(descripInfo);
    }
    return null;
  }

  saveOpciones(opciones: OptionsI[]): void{
    const encryToken = CryptoJS.AES.encrypt(
      JSON.stringify(opciones), 
      this._strKeyOpciones
    ).toString();
    localStorage.setItem(this._strOpciones, encryToken);
  } 

  getOpciones(): OptionsI[] | null{
    const encryptInfo = localStorage.getItem(this._strOpciones);
    if(encryptInfo){
      var descripInfo = CryptoJS.AES.decrypt(
        encryptInfo,
        this._strKeyOpciones
      ).toString(CryptoJS.enc.Utf8);
      return JSON.parse(descripInfo);
    }
    return null;
  }

}
