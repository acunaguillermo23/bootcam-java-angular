import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from './pages/user/user.component';
import { PrimeBlocksModule } from 'src/app/demo/components/primeblocks/primeblocks.module';
import { UserRoutingModule } from './user-routing.module';
import { UtilitiesModule } from 'src/app/modules/components/utilities/utilities.module';



@NgModule({
  declarations: [UserComponent],
  imports: [
    CommonModule,
    PrimeBlocksModule,
    UserRoutingModule,
    UtilitiesModule
  ],
})
export class UserModule { }
