import { Component } from '@angular/core';
import { ComboI } from 'src/app/modules/interfaces/general.interface';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrl: './user.component.scss',
  standalone: false
})
export class UserComponent {
  private _strHeaderName : string = "Mantenimiento de Usuario";
  private _flagButton1: boolean = true;

  private _arrayTypeStatus: ComboI[];


  constructor(){
    this._arrayTypeStatus = [
      {
        label: 'Activo',
        value: {
          id: 1,
          name: 'Activo',
          code: 'a'
        }
      },
      {
        label: 'Inactivo',
        value: {
          id: 2,
          name: 'Inactivo',
          code: 'i'
        }
      }
    ]
  }

  public get getStrHeaderName(): string {
    return this._strHeaderName;
  }

  public get getViewButton1(): boolean {
    return this._flagButton1;
  }

  public get getArrayTypeStatus(): ComboI[]{
    return this._arrayTypeStatus;
  }


  onClickNewUser(event: string){
    console.log("Nuevo usuario", event);
  }

  filterStatus(status: string){
    console.log(status)
  }

  filterName(name: string){
    console.log(name)
  }

  filterEmail(email: string){
    console.log(email)
  }

  filterUser(user: string){
    console.log(user)
  }
}
