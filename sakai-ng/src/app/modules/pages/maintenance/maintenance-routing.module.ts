import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';



@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'usuario',
        loadChildren: () => import('./user/user.module').then((m) => m.UserModule)
      },
      // {
      //   path: 'productos',
      //   loadChildren: () => import('./product/product.module').then((m) => m.UserModule)
      // }
      {
        path: '**', redirectTo: ''
      }
    ])
  ],
  exports:[RouterModule]
})
export class MaintenanceRoutingModule { }
