import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ComboI } from 'src/app/modules/interfaces/general.interface';

@Component({
  selector: 'app-serach-drop-down-status',
  standalone: false,
  templateUrl: './app-serach-drop-down-status.component.html',
  styleUrl: './app-serach-drop-down-status.component.scss'
})
export class AppSerachDropDownStatusComponent implements OnInit{

  @Input('comboStatus') arrayComboStatus: ComboI[] = [];
  @Output() eventValueStatus = new EventEmitter<string>();

  formStatusDrop = new FormControl();

  ngOnInit(){
    this.formStatusDrop.valueChanges.subscribe((e) => {  
      if(e) {
        console.log(e)
        this.eventValueStatus.emit(e.name)
      }
      else this.eventValueStatus.emit();
    });
  }

}

