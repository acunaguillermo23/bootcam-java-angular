import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'app-search-input',
  standalone: false,
  templateUrl: './search-input.component.html',
  styleUrl: './search-input.component.scss'
})
export class SearchInputComponent {
  @ViewChild('filter') filter!: ElementRef<HTMLInputElement>;

  @Input() strLabel!: string ;

  @Output() eventValueInput = new EventEmitter<string>();

  private _flagSearchIcon: boolean  = true;
  private _flagClearIcon: boolean = false;

  public get getflagSearchIcon(): boolean {
    return this._flagSearchIcon;
  }

  public get getflagClearIcon(): boolean{
    return this._flagClearIcon;
  }

  getTextInput(event: Event){
    var value = (event.target as HTMLInputElement).value;

    if(value.length > 0){
      this.changeIconFilter(true, false);
    }else{
      this.changeIconFilter(false, true);
    }

    this.eventValueInput.emit(value);
  }

  clearFilter(flag: boolean){
    if(flag){
        this.eventValueInput.emit('');
        this.filter.nativeElement.value = '';
        this.changeIconFilter(false, true);
    }
  }

  changeIconFilter(clearIcon: boolean, searchIcon: boolean){
    this._flagClearIcon = clearIcon;
    this._flagSearchIcon = searchIcon;
  }
}
