import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppSerachDropDownStatusComponent } from './app-serach-drop-down-status/app-serach-drop-down-status.component';
import { DropdownModule } from 'primeng/dropdown';
import { ReactiveFormsModule } from '@angular/forms';
import { SearchInputComponent } from './search-input/search-input.component';
import { InputTextModule } from 'primeng/inputtext';

@NgModule({
  declarations: [AppSerachDropDownStatusComponent,SearchInputComponent],
  exports: [AppSerachDropDownStatusComponent,SearchInputComponent],
  imports: [
    CommonModule,
    DropdownModule,
    InputTextModule,
    ReactiveFormsModule
  ]
})
export class UtilitiesModule { }
