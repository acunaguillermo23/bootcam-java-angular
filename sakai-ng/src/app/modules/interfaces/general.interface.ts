export interface ComboI<T = any>{
    label?: string;
    value: T;
    styleClass?: string;
    icon?:string;
    tittle?: string;
    disabled?: boolean;
}